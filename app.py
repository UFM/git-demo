import time
from colorama import Fore, Back, Style

count=0

while True:
    print(Fore.BLUE+f"[ {count} ]")
    print(Style.RESET_ALL)
    time.sleep(1)
    count+=1

